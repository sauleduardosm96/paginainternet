<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class subastaController extends Controller
{
  public function index()
  {
    $sucursales=array("mp_miahuatlan","mp_abastos","mp_xoxo",
    "mp_modulo_azul","mp_matriz","mp_tehuantepec",
    "mp_salinacruz","mp_ixtepec","mp_juchitan13",
    "mp_juchitan14","mp_sanblas","mp_matias",
    "mp_pochutla","mp_puerto_escondido","mp_pinotepa",
    "mp_tlacolula","mp_judicial","mp_huajuapan",
    "mp_tlaxiaco","mp_20","mp_tuxtepec","mp_loma_bonita");
    $subastas=array();

    foreach ($sucursales as $s) {
      $sucsub=DB::connection($s)->select('select * from c_fecha_subasta
      INNER JOIN c_sucursal ON c_fecha_subasta.c_sucursal_id = c_sucursal.id
      where fecha_subasta >= CURDATE()');
      array_push($subastas,$sucsub);
    }
    $subs=array();
    foreach($subastas as $s){
      foreach ($s as $a) {
        array_push($subs,$a);
      }
    }
    usort($subs, array($this, "ordenar"));


    /*$client = new Client([
        // Base URI is used with relative requests
        'base_uri' => 'http://187.217.222.243:83/api/',
        // You can set any number of default request options.
        'timeout'  => 2.0,
    ]);
    $a = $client->request('GET', 'apisubastas');*/
    //$subs=json_decode($a->getBody()->getContents());



    return view('paginaPrincipal.index',compact('subs'));
  }
  function ordenar( $a, $b ) {
    return strtotime($a->fecha_subasta) - strtotime($b->fecha_subasta);
  }
    public function subastas()
    {
      $sucursales=array("mp_miahuatlan","mp_abastos","mp_xoxo",
      "mp_modulo_azul","mp_matriz","mp_tehuantepec",
      "mp_salinacruz","mp_ixtepec","mp_juchitan13",
      "mp_juchitan14","mp_sanblas","mp_matias",
      "mp_pochutla","mp_puerto_escondido","mp_pinotepa",
      "mp_tlacolula","mp_judicial","mp_huajuapan",
      "mp_tlaxiaco","mp_20","mp_tuxtepec","mp_loma_bonita");
      $subastas=array();

      foreach ($sucursales as $s) {
        $sucsub=DB::connection($s)->select('select * from c_fecha_subasta
        INNER JOIN c_sucursal ON c_fecha_subasta.c_sucursal_id = c_sucursal.id
        where fecha_subasta >= CURDATE()');
        array_push($subastas,$sucsub);
      }
      $subs=array();
      foreach($subastas as $s){
        foreach ($s as $a) {
          array_push($subs,$a);
        }
      }
      usort($subs, array($this, "ordenar"));

      /*$client = new Client([
          // Base URI is used with relative requests
          'base_uri' => 'http://187.217.222.243:83/api/',
          // You can set any number of default request options.
          'timeout'  => 2.0,
      ]);
      $a = $client->request('GET', 'apisubastas');*/
      //$subs=json_decode($a->getBody()->getContents());
      return view('paginaPrincipal.subastas',compact('subs'));

    }
    public function estado_boleta(Request $req){

      if ($req->sucursal!=null && $req->fecha!=null && $req->id!=null){
          $consulta="select tipo from t_boleta inner join c_status_empenio ON t_boleta.c_status_empenio_id=c_status_empenio.id where t_boleta.id=$req->id AND fecha_limite_pago='$req->fecha' ";
          switch($req->sucursal){
            case "15":
              $boleta=DB::connection("mp_matriz")->select($consulta);

            break;
            case "16":
             $boleta=DB::connection("mp_tehuantepec")->select($consulta);
            break;
            case "19":
             $boleta=DB::connection("mp_juchitan13")->select($consulta);
            break;
            case "20":
             $boleta=DB::connection("mp_juchitan14")->select($consulta);
            break;
            case "22":
             $boleta=DB::connection("mp_matias")->select($consulta);
            break;
            case "18":
             $boleta=DB::connection("mp_ixtepec")->select($consulta);
            break;
            case "21":
             $boleta=DB::connection("mp_sanblas")->select($consulta);
            break;
            case "12":
             $boleta=DB::connection("mp_abastos")->select($consulta);
            break;
            case "28":
             $boleta=DB::connection("mp_huajuapan")->select($consulta);
            break;
            case "31":
             $boleta=DB::connection("mp_tuxtepec")->select($consulta);
            break;
            case "24":
             $boleta=DB::connection("mp_puerto_escondido")->select($consulta);
            break;
            case "13":
             $boleta=DB::connection("mp_xoxo")->select($consulta);
            break;
            case "17":
             $boleta=DB::connection("mp_salinacruz")->select($consulta);
            break;
            case "30":
             $boleta=DB::connection("mp_20")->select($consulta);
            break;
            case "32":
             $boleta=DB::connection("mp_loma_bonita")->select($consulta);
            break;
            case "25":
             $boleta=DB::connection("mp_pinotepa")->select($consulta);
            break;
            case "14":
             $boleta=DB::connection("mp_modulo_azul")->select($consulta);
            break;
            case "27":
             $boleta=DB::connection("mp_judicial")->select($consulta);
            break;
            case "23":
             $boleta=DB::connection("mp_pochutla")->select($consulta);
            break;
            case "29":
             $boleta=DB::connection("mp_tlaxiaco")->select($consulta);
            break;
            case "26":
             $boleta=DB::connection("mp_tlacolula")->select($consulta);
            break;
            case "11":
             $boleta=DB::connection("mp_miahuatlan")->select($consulta);
            break;
          }
          return $boleta;
      }
         return 502;
/*
         $client = new Client([
             // Base URI is used with relative requests
             'base_uri' => 'http://187.217.222.243:83/api/',
             // You can set any number of default request options.
             'timeout'  => 2.0,
         ]);
         $a = $client->request('POST', 'apiboletas',['request'=>$req]);*/
         return $boleta;




    }



}
