	@section('header')
  <!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Monte de Piedad del Estado de Oaxaca</title>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/business-casual.css" rel="stylesheet">
    <script
  src="http://code.jquery.com/jquery-3.3.1.js"
  integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
  crossorigin="anonymous"></script>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Biryani:400,700" rel="stylesheet">
     <script src="alerts/dist/sweetalert2.min.js"></script>
       <link rel="stylesheet" type="text/css" href="alerts/dist/sweetalert2.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
  <script type="text/javascript">
   //


  </script>


<body>

    <div class="brand">
      <div class="container">
               <div class="row flex-parent">
	<div class="col-xs-2">
		<a href="index.php"><img src="img/iconos/monte-de-piedad-logo.png" class="img-responsive center-block" /></a>
	</div>

	<div class="col-xs-1 text-right">
		<p class="texto-header">Horario de Servicio</p></div>
		<div class="col-xs-1 text-center">
		<img src="img/iconos/horario.png" width="58" height="58" alt=""/> </div>
		<div class="col-xs-2 flex-child text-left"><p class="texto-header">8:30 a 16:00 h | Lunes a Viernes <br>
Sábados | 9:00 a 13:00 h
<br>
		</p>
		 </div>

		<div class="col-xs-1 flex-child text-right"><p class="texto-header">Preguntas<br>
Frecuentes </p>
		</div>
		<div class="col-xs-1 text-center">
		<a href="faqs.php"><img src="img/iconos/faqs-header.png" width="58" height="58" alt=""/></a> </div>
		<div class="col-xs-1 flex-child text-center">

<ul class="nav nav-pills" style="margin-top: 10px">
  <li><a href="contacto.php" style="color: #B4106B">Buzón</a></li>
</ul>


		</div><div class="col-xs-1">
		</div><div class="col-xs-1 flex-child text-center">
		<a href="https://twitter.com/mpiedad_goboax?lang=es" target="_blank"><img src="img/iconos/twitter.png" width="58" height="58" alt=""/></a> </div><div class="col-xs-1 flex-child text-center">
		<a href="https://www.facebook.com/Monte-de-Piedad-del-Estado-de-Oaxaca-170349739685921/" target="_blank"><img src="img/iconos/facebook.png" width="58" height="58" alt=""/></a> </div>
</div>



          </div>
</div>
<!-- Navigation -->
    <nav class="navbar navbar-default" role="navigation">
        <div class="container">

            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- navbar-brand is hidden on larger screens, but visible when the menu is collapsed -->
                <a class="navbar-brand" href="index.php"> <img class="img-responsive" src="img/iconos/monte-de-piedad-logo.png"alt=""/></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                  <li>
                        <a href="historia.php">Historia</a>
                    </li>
                   <li class="dropdown">

       <div class='btn-group'>

     <!-- Configurar el botón dropdown -->

       <button type="button" class="btn btn-default3 dropdown-toggle"
            data-toggle="dropdown">Servicios<span class="caret"></span>
    </button>

        <!-- Configurar el botón dropdown -->

     <!-- Lista de opciones -->
            <ul class="dropdown-menu">
                   <li><a href="empenos.php">¿Cómo empeño?</a></li>
          <li class="divider"></li>

          <li><a href="articulos.php">¿Qué puedo empeñar?</a></li>
          <li class="divider"></li>
          <li><a href="ventajas.php">Ventajas de empeñar<br>
con nosotros</a></li><li class="divider"></li><li><a href="pagos.php">Movimientos a tu boleta</a></li>
        </ul>
    <!-- Lista de opciones -->

     </div>
            </li>




                    <li class="dropdown" id="accountmenu">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Sucursales<b class="caret"></b></a>
                    <ul class="dropdown-menu">

                        <li class="dropdown-submenu">
                          <a tabindex="-1" href="sucursales.php#valles">Valle</a>
                          <ul class="dropdown-menu">
                            <li><a tabindex="-1" href="sucursales.php#matriz">Matriz</a></li>
                            <li> <a href="sucursales.php#abastos">Abastos</a></li>
                            <li><a href="sucursales.php#xoxo">Xoxo</a></li>
                            <li><a href="sucursales.php#modulo-azul">Módulo azul</a></li>
                            <li><a href="sucursales.php#reforma">Reforma</a></li>
                            <li><a href="sucursales.php#cdjudicial">Ciudad Judicial</a></li>
                            <li><a href="sucursales.php#tlacolula">Tlacolula</a></li>
                          </ul>
                        </li>
                         <li class="divider"></li>
                         <li class="dropdown-submenu">
                          <a tabindex="-1" href="sucursales.php#istmo">Istmo</a>
                          <ul class="dropdown-menu">
                            <li><a tabindex="-1" href="sucursales.php#sanblas">San Blas</a></li>
                            <li> <a href="sucursales.php#matias">Matías Romero</a></li>
                            <li><a href="sucursales.php#juchitan13">Juchitán 13</a></li>
                           <li><a href="sucursales.php#juchitan14">Juchitán 14</a></li>
                            <li><a href="sucursales.php#ixtepec">Cd. Ixtepec</a></li>
                             <li> <a href="sucursales.php#salinacruz">Salina Cruz</a></li>
                           <li><a href="sucursales.php#tehuantepec">Tehuantepec</a></li>
                            </ul>
                        </li>
                        <li class="divider"></li>
                         <li class="dropdown-submenu">
                          <a tabindex="-1" href="sucursales.php#costa">Costa</a>
                          <ul class="dropdown-menu">
                            <li><a tabindex="-1" href="sucursales.php#puerto-escondido">Puerto Escondido</a></li>
                            <li><a href="sucursales.php#pochutla">Pochutla</a> </li>
                            <li><a href="sucursales.php#pinotepa">Pinotepa</a></li>
                          </ul>
                        </li>
                        <li class="divider"></li>
                         <li class="dropdown-submenu">
                          <a tabindex="-1" href="sucursales.php#mixteca">Mixteca</a>
                          <ul class="dropdown-menu">
                            <li><a tabindex="-1" href="sucursales.php#tlaxiaco">Tlaxiaco</a></li>
                            <li><a href="sucursales.php#huajuapan">Huajuapan</a></li>
                          </ul>
                        </li>
                        <li class="divider"></li>
                         <li class="dropdown-submenu">
                          <a tabindex="-1" href="sucursales.php#cuenca">Cuenca</a>
                          <ul class="dropdown-menu">
                            <li><a tabindex="-1" href="sucursales.php#lomabonita">Loma Bonita</a></li>
                            <li> <a href="sucursales.php#tuxtepec">Tuxtepec</a></li>
                          </ul>
                        </li>

                    </ul>
                </li>
    <!-- Lista de opciones -->


                    <li>
                        <a href="boletas.php">Consulta de boletas</a>
                    </li>

                    <li>

                        <a href="tienda.php">Artículos en Venta</a>
                    </li>
                    <li>
                        <a href="subastas.php">Subastas</a>
                    </li>
                    <li>
                        <a href="http://modulotransparencia.montedepiedad.gob.mx/">Transparencia</a>
                    </li>
                </ul>
            </div>


            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    <!-- Navigation -->

    @endsection
