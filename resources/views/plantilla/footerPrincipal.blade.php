@section('footer')
      <footer><div class="azul"><div class="container">
   <div class="row">
     <div class="col-lg-6 col-md-6">
       
       <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right" style="padding: 20px"><h2>Monte de Piedad<br>
         del Estado de Oaxaca</h2><br>
        <!--<img src="img/iconos/logo-oaxaca.png" width="198" height="84" alt=""/> -->
         
         
         </div>
       <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-left"><br>
         <h4>Oficina Matriz<br>
           Avenida Morelos 703,<br>
           C.P. 68000, Centro,<br>
           Oaxaca de Juárez, Oaxaca.<br>
           
           Tel. 01-951-501 62 67 <br><br><br>
          <a style="color: white" href="{{url('/terminos')}}">Aviso de privacidad</a> 
           </h4>
         </div>
       
     </div>   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        
<ul class="info-fo"> <li><a href="{{url('/directorio')}}">Directorio</a></li>
<li><a href="{{url('/historia')}}">Historia</a></li>
<li><a href="http://trans.montedepiedad.gob.mx/">Transparencia</a></li>
<li><a href="http://trans.montedepiedad.gob.mx/fiscal">Financiera</a></li>

<li ><a style="border: none" href="http://consultapublicamx.inai.org.mx:8080/vut-web/?idSujetoObigadoParametro=11883&idEntidadParametro=20&idSectorParametro=21"><img width="180px" style="margin-left: 10px; margin-top: 0px" src="img/snt.svg"></a></li>
<li><a href="{{url('/codigoEtica')}}">Código de ética de la Función Pública.</a></li>
<li><a href="{{url('/valores')}}">Video.</a></li>
</ul>
   
   </div>
   </div>
   

</div></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center small">
                    <p>Monte de Piedad del Estado de Oaxaca. &copy; Todos los derechos reservados</p>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

 
@endsection

