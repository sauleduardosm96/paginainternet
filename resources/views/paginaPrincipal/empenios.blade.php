@extends ('main')
@include('plantilla.headerPrincipal')

@section('contenido')


       
       <div class="container">

       <div class="row">
          <div class="col-lg-12"><img class="img-responsive" src="img/banners/empenos.jpg" /></div>
        </div>
       <div class="row">
  <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12" style="height: 100px;background:#a0df53;padding-top: 20px; ">
    <h2 class="text-center">Empeñar con nosotros es muy fácil</h2>
    
  </div>
       </div>
       <div class="col-lg-9">
  <div class="row pasos" style="margin-top:20px;">
    
    
    <div class="col-lg-5 col-md-6 col-sm-6 col-xs-12 moradoclaro pasos">
      <img class="img-responsive img-border img-center" src="img/iconos/pasos/1.png"/>
      <h3 class="text-center">Trae tu artículo a  alguna de nuestras  22 sucursales.
        </h3>
      
      </div>
    <div class="col-lg-6">
      <p class="texto-empenos">Lleva a cualquiera de las 22 sucursales o a la oficina matriz tu prenda a empeñar, acompañada de una identificación oficial vigente (INE, Cartilla, Licencia de conducir).</p>
      </div>
    
  </div>
         <div class="row" style="padding-top: 20px">
           <div class="col-lg-5 col-md-6 col-sm-6 col-xs-12 moradoclaro">
             <img class="img-responsive img-border img-center" src="img/iconos/pasos/02.png"/>
             <h3 class="text-center">Un experto te dirá cuanto te podemos ofrecer de préstamo </h3>
             </div><div class="col-lg-6">
               <p class="texto-empenos"> Pasa a la ventanilla de <strong>Avalúo</strong> y entrega tu artículo al <strong>Valuador</strong>. Él lo examinará, te hablará sobre su estado y te dirá cuánto dinero te podemos prestar.</p>
               </div>
           </div>
         <div class="row" style="padding-top: 20px">
           <div class="col-lg-5 col-md-6 col-sm-6 col-xs-12 moradoclaro">
             <img class="img-responsive img-border img-center" src="img/iconos/pasos/03.png"/>
             <h3 class="text-center">Recibe tu dinero de inmediato y en efectivo </h3>
           </div>
           <div class="col-lg-6">
             <p class="texto-empenos">Recibe en caja el monto del préstamo ofrecido en efectivo.<br><br>
               
             La prenda queda resguarda en “<strong>Depositaría</strong>” durante el plazo que esté activo tu préstamo </p>
             </div>
          </div><br><br>
       </div>
       <div class="col-lg-3 derecha-empeños">
		   <div class="row">   <p >Puedes refrendar o abonar tu boleta en cualquier momento y cuantas veces quieras.<br></p>
              <hr>
            <p>Una vez hecho el movimiento (refrendo o abono) se te entrega una nueva boleta con el mismo plazo que la boleta original.</p><br>
     <hr>
           <p>
            Los abonos o refrendos son ilimitados. <br>
            </p>
           <hr>
           <p>
            
            Cuando empeñes alhajas puedes elegir desde un 75 hasta un 85% de préstamo sobre el valor de tu prenda, con un plazo de 4 o 12 semanas.</p>
         </div>
       </div>
       
    		</body>


@endsection

@include('plantilla.footerPrincipal')