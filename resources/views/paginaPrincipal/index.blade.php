@extends ('main')
@include('plantilla.headerPrincipal')




@section('contenido')
    <div class="container">

      <div class="modal fade" id="modalSorteo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h2 class="modal-title text-danger text-center" id="exampleModalLabel">Billete Conmemorativo</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <img class="img-responsive img-full" src="img/boleto.jpg" alt="">
                    <br>
                    Billete conmemorativo del "86 aniversario del monte de piedad del estado de Oaxaca". Sorteo mayor a celebrarse el martes 20 de agosto de 2019 a las 20 horas.
                    Un premio en 3 series de 6,000,000.00 cada una. Adquiére tu billete en el punto de venta de tu preferencia o en las sucursales de esta Institución. También puedes adquirirlo  aquí: <a href="https://web.tulotero.mx" target="_blank">web.tulotero.mx</a> o en <a href="https://www.cmillonario.com" target="_blank">www.cmillonario.com</a>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <a type="button" href="{{url('/sorteoMonte')}}" class="btn btn-primary">Ver más...</a>
                  </div>
                </div>
              </div>
            </div>




        <div class="row">
          <!-- <div class="col-lg-12 visible-xs text-center">



                    <div id="carousel-example-generic" class="carousel slide">

                        <ol class="carousel-indicators hidden">
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>

                        </ol>

                        <div class="carousel-inner">
                            <div class="item active">
				<img class="img-responsive img-full" src="img/banners/85ciento.jpg" alt="">
                            </div>
                            <div class="item">
                                <img class="img-responsive img-full" src="img/banners/1ciento.jpg" alt="">
                            </div>

                      </div>



                        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                            <span class="icon-prev"></span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                            <span class="icon-next"></span>
                        </a>




                    </div>

                </div> -->
                 </a>
                <div class="col-lg-12 hidden-xs text-center">
                    <div id="carousel-example-generic" class="carousel slide">

                        <ol class="carousel-indicators hidden">
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>

                        </ol>


                        <div class="carousel-inner">

                            <div class="item active">
                              <img class="img-responsive img-full" src="img/iconos/banner-montedepiedad.jpg" alt="">
                            </div>
                            <div class="item">
                                <img class="img-responsive img-full" src="img/banner2.jpg" alt="">
                            </div>

                      </div>


                        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                            <span class="icon-prev"></span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                            <span class="icon-next"></span>
                        </a>


                    </div>

                </div>

        </div>

      <div class="row pasos">

                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 moradofuerte">
                    <h2 class="text-center">Empeñar con nosotros es muy fácil</h2>

                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 moradoclaro">
                 <img class="img-responsive img-border img-center" src="img/iconos/pasos/1.png"/>
                  <h3 class="text-center">Trae tu prenda a  una de las  22 sucursales u oficina Matriz
                    </h3>

              </div>
                  <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 moradoclaro">
                     <img class="img-responsive img-border img-center" src="img/iconos/pasos/02.png"/>
                     <h3 class="text-center">Un experto te dirá cuánto te pueden ofrecer de préstamo </h3>
                 </div>
                  <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 moradoclaro">
                     <img class="img-responsive img-border img-center" src="img/iconos/pasos/03.png"/>
                     <h3 class="text-center">Recibe tu dinero de inmediato y en efectivo </h3>
                 </div>

      </div>
	  <div class="row"><div class="col-lg-6 col-md-6 col-xs-12 faqs"><img src="img/ventajas.png"alt=""/>
    	    <p>¿Ya conoces las ventajas de empeñar con nosotros?</p>
    	    <a class="ventajas-boton" href="{{url('/ventajas')}}">Todas las ventajas</a>
      </div>

      <div class="col-lg-6 col-md-6 col-sm-12 boleta">
                  <h2 class="text-center">Consulta el estado de tu boleta</h2>

      </div>

<div class="col-lg-6 col-md-6 col-sm-12 boleta2">

      <!-- <form role="form" action="#!" method="post"> -->
      <div class=" col-lg-4 col-md-6 col-sm-5 form-group">
        <input type="number" class="form-control" id="boleta_id" placeholder="Número de Boleta" name="boleta" required="true">

      </div>
      <div class="col-lg-1 col-sm-1"></div>

      <div class=" col-lg-3 col-md-6 col-sm-5 form-group">
        <input type="date" id="fecha_id" class="form-control" name="fecha" required="true">
         <label class="vencimiento2">Fecha de vencimiento </label>
      </div>

      <div class="col-lg-1 col-sm-1"></div>





      <div class="form-group col-lg-3 col-md-6 col-sm-5">

            <select class="form-control" id="sucursal_id" name="sucursal_id" required>
              <option value="15">Oficina Matriz</option>
              <option value="16">Tehuantepec</option>
              <option value="19">Juchitan 13 </option>
              <option value="20">Juchitan 14</option>
              <option value="22">Matias Romero</option>
              <option value="18">Cd. Ixtepec</option>
              <option value="21">San Blas</option>
              <option value="12">Abastos</option>
              <option value="28">Huajuapan</option>
              <option value="31">Tuxtepec</option>
              <option value="24">Puerto Escondido</option>
              <option value="13">Xoxo</option>
              <option value="17">Salina Cruz</option>
              <option value="30">20 de nov.</option>
              <option value="32">Loma Bonita</option>
              <option value="25" >Pinotepa</option>
              <option value="14">Modulo Azul</option>
              <option value="27">Cd. Judicial</option>
              <option value="23">Pochutla</option>
              <option value="29">Tlaxiaco</option>
              <option value="26">Tlacolula</option>
              <option value="11">Miahuatlan</option>
           </select>
      </div>
      <div class="col-lg-9">
      </div>
      <div class="col-lg-3">
          <a href="#!" class="btn btn-default" onclick="verBoleta();">Enviar</a>

      </div>
</form>


            </div>



      </div>
<style>
div.ex3 {
  width: 100%;
  height: 40%;
  overflow: auto;
}
</style>

		<div class="row">
			<div class="col-lg-6 col-md-6 subastas" >
        <a href="{{url('/subastas')}}">
          <h2 class="proximas text-center">
            <img style="margin-right:15px; " src="img/iconos/subastas.png" width="58" height="58">Próximas Subastas
          </h2>
        </a>

        <a href="{{url('/subastas')}}"><img src="img/flecha-subastas.png" width="100%" height="51" alt=""/></a>
        <div class="ex3">
          <table width="90%" border="0" cellspacing="0" cellpadding="10" style="margin: 0 auto">

  <tbody id="subastasIndex_id">
    <tr>
      <td colspan="3" style="text-align: center">

      </td>
    </tr>
@if(isset($subs))
    @foreach($subs as $s)
    <tr class='prox-sub'>
      <td class='sucursal-prox'>{{$s->sucursal}}</td>
      @if($s->c_tipo_subasta==1)
        <td>ALHAJAS</td>
      @else
        <td>VARIOS</td>
      @endif
      <td>{{date("d/m/Y", strtotime($s->fecha_subasta))}}</td>
    </tr>
    @endforeach
@endif


    <tr class="prox-sub">
      <td colspan="3" style="text-align: center">

      </td>
    </tr>
  </tbody>

</table>
</div>
  <a href="{{url('/subastas')}}"><img src="img/flecha-subastas-abajo.png" width="100%" height="51" alt=""/></a>
</div><div class="col-lg-3 col-md-3  col-xs-6 articulos">
                  <h2 class="text-center">¿Qué Artículos puedo empeñar?
        </h2>
                                <a href="{{url('/articulos')}}"><img class="img-responsive img-border" src="img/productos.png"/></a>

              </div>
	    <div class="col-lg-3 col-md-3 col-xs-6 iconinfo">
	      <p><a href="{{url('/preguntas')}}"><img src="img/iconos/icono-faq-grande.png" width="113" height="111" class="img-responsive img-border" /></a></p>
	      <p>Preguntas<br>
Frecuentes</p>
	    </div>

    </div>

    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p id="texto" class="text-dark">

            </p>
          </div>
          <div class="modal-footer">
            <a href="#!" class="btn btn-default" onclick="cerrarModal();">Cerrar</a>
          </div>
        </div>
      </div>
    </div>






    <script>

    $( document ).ready(function() {
        $('#modalSorteo').modal('show');
    });

    function verBoleta() {

      let id=$('#boleta_id').val();
      let fecha=$('#fecha_id').val();
      let sucursal=$('#sucursal_id').val();
      $('#exampleModal').modal('show');

      $.post('/estadoboleta',{id:id,sucursal:sucursal,fecha:fecha,_token: '{{csrf_token()}}' },function(d) {

        console.log(d);
        if(d==502){
          $('#texto').empty();
          $('#exampleModalLabel').empty();
          $('#texto').append("La información ingresada es incorrecta.");
          $('#exampleModalLabel').append("Error");
        }else{

          let estadoBoleta=d[0]['tipo'];
          $('#texto').empty();
          $('#exampleModalLabel').empty();
          $('#texto').append("El estado de su boleta es: ".concat(estadoBoleta));
          $('#exampleModalLabel').append("Estado de la boleta");
        }
      });


    }

    function cerrarModal(){
      $('#exampleModal').modal('hide');
      $('#texto').empty();
      $('#exampleModalLabel').empty();
    }





    </script>
	</body>




 @include('plantilla.footerPrincipal')
@endsection

<!--
<!DOCTYPE html>
<html>
<head>
  <title>FUERA DE SERVICIO</title>
</head>
<body>

  <H1>REGRESAMOS PRONTO</H1>

</body>
</html>

-->
