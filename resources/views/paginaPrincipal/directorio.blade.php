@extends('main')
@include('plantilla.headerPrincipal')

@section('contenido')
<div class="container">

  <div class="row">
          <div class="col-lg-12"><img class="img-responsive" src="img/directorio.jpg" /></div>
     </div>
  <div class="row">
   <div class=" col-lg-12"  style="height: 100px; background:#f5780f; padding-top: 2%; margin-bottom: 30px; text-align: center"><h2>Directorio</h2></div>

<div id="exTab3" class="container">
<ul  class="nav nav-pills">
      <li class="active">
        <a  href="#1b" data-toggle="tab">Oficina Matriz</a>
      </li>
      <li><a href="#2b" data-toggle="tab">Valles</a>
      </li>
      <li><a href="#3b" data-toggle="tab">Istmo</a>
      </li>
      <li><a href="#4b" data-toggle="tab">Costa</a>
      </li><li><a href="#5b" data-toggle="tab">Cuenca</a>

      </li><li><a href="#7b" data-toggle="tab">Mixteca</a>
      </li>
    </ul>

      <div class="tab-content clearfix">
        <div class="tab-pane active" id="1b">

           <div class=" col-lg-12"  style="height: 100px; background:#24A093; padding-top: 2%; margin-bottom: 30px; text-align: center"><h2>Oficina matriz</h2></div>

<div class="col-lg-6 col-lg-offset-0 col-md-6 col-md-offset-0 direc">

      <p><strong>LIC. EDGARDO ALEJANDRO AGUILAR ESCOBAR.</strong></p>
      <em>DIRECTOR GENERAL.</em></p>
      <p>TELÉFONO OFICIAL: 5016267 EXT. 129<br>
        CORREO ELECTRÓNICO OFICIAL: <a href="mailto:direccion@montedepiedad.gob.mx">direccion@montedepiedad.gob.mx</a></p>
      <p>DIRECCIÓN OFICIAL: Morelos #703<br>
        COLONIA: Centro<br>
        C.P 68000<br>
       </p><hr>
     </div>

<div class="col-lg-6 col-lg-offset-0 col-md-6 col-md-offset-0 direc">

      <p><strong>MTRO. JUAN CARLOS ZARATE MORENO</strong></p>
      <em>DIRECTOR ADMINISTRATIVO.</em></p>
      <p>TELÓFONO OFICIAL: 5016267 EXT. 130<br>
        CORREO ELECTRÓNICO OFICIAL:  <a href="mailto:administracion@montedepiedad.gob.mx"> administracion@montedepiedad.gob.mx</a></p>
      <p>DIRECCIÓN OFICIAL: Morelos #703<br>
        COLONIA: Centro<br>
        C.P 68000<br>
       </p>
      <hr>     </div>

<div class="col-lg-6 col-lg-offset-0 col-md-6 col-md-offset-0 direc">

      <p><strong>LIC. ALAN HUMBERTO HERNÁNDEZ GONZÁLEZ.</strong></p>
      <em>ASESOR.</em></p>
      <p>TELÓFONO OFICIAL: 5016267 EXT. 133<br>
        CORREO ELECTRÓNICO OFICIAL:  <a href="mailto:asesor@montedepiedad.gob.mx">asesor@montedepiedad.gob.mx</a></p>
      <p>DIRECCIÓN OFICIAL: Morelos #703<br>
        COLONIA: Centro<br>
        C.P 68000<br>
       </p>
      <hr>
</div>

<div class="col-lg-6 col-lg-offset-0 col-md-6 col-md-offset-0 direc">

      <p><strong></strong>
       <p><strong>LIC. VICTOR ALBERTO PORRAS ARELLANO.</strong></p>
        <em>JEFE DEL DEPARTAMENTO JURÍDICO.</em></p>
      <p>TELÉFONO OFICIAL: 5016267 EXT. 124<br>
        CORREO ELECTRÓNICO OFICIAL:  <a href="mailto:juridico@montedepiedad.gob.mx">juridico@montedepiedad.gob.mx</a></p>
      <p>DIRECCIÓN OFICIAL: Morelos #703<br>
        COLONIA: Centro<br>
        C.P 68000<br>
       </p>
      <hr>
  </div>

<div class="col-lg-6 col-lg-offset-0 col-md-6 col-md-offset-0 direc">

      <p><strong>LIC. MAYRA TORRES VÁSQUEZ.</strong>
        <em>JEFA DE LA UNIDAD DE PLANEACIÓN Y MERCADOTÉCNIA.</em></p>
        <p>TELÉFONO OFICIAL: 5016267 EXT. 132<br>
        CORREO ELECTRÓNICO OFICIAL:  <a href="mailto:planeacion@montedepiedad.gob.mx">planeacion@montedepiedad.gob.mx</a></p>
      <p>DIRECCIÓN OFICIAL: Morelos #703<br>
        COLONIA: Centro<br>
        C.P 68000<br>
   </p>
  <hr>
</div>


<div class="col-lg-6 col-lg-offset-0 col-md-6 col-md-offset-0 direc">
      <p><strong>C.P. ISANA VÁZQUEZ MARTÍNEZ.</strong>
        <em>JEFA DEL DEPARTAMENTO DE MERCADOTÉCNIA.</em></p>
      <p>TELÉFONO OFICIAL: 5016267 EXT. 132<br>
        CORREO ELECTRÓNICO OFICIAL:<a href="mailto:mercadotecnia@montedepiedad.gob.mx">mercadotecnia@montedepiedad.gob.mx</a></p>
      <p>DIRECCIÓN OFICIAL: Morelos #703<br>
        COLONIA: Centro<br>
        C.P 68000<br>
       </p>
       <hr>
     </div>

    <div class="col-lg-6 col-lg-offset-0 col-md-6 col-md-offset-0 direc">
      <p><strong>C. LUIS ANTONIO HERNÁNDEZ PÉREZ</strong>
        <em>JEFE DEL DEPARTAMENTO DE PLANEACIÓN.</em></p>
      <p>TELÉFONO OFICIAL: 5016267 EXT. 131<br>
        CORREO ELECTRÓNICO OFICIAL:<a href="mailto:presupuesto@montedepiedad.gob.mx">presupuesto@montedepiedad.gob.mx</a></p>
      <p>DIRECCIÓN OFICIAL: Morelos #703<br>
        COLONIA: Centro<br>
        C.P 68000<br>
       </p>
       <hr>
     </div>

<div class="col-lg-6 col-lg-offset-0 col-md-6 col-md-offset-1 direc">

      <p><strong>LIC. MARIA FERNANDA GUZMAN PRADO.</strong>
        <em>JEFA DEL DEPARTAMENTO DE DONATIVOS.</em></p>
      <p>TELÉFONO OFICIAL: 5016267 EXT. 107<br>
        CORREO ELECTRÓNICO OFICIAL:  <a href="mailto:donaciones@montedepiedad.gob.mx">donaciones@montedepiedad.gob.mx</a></p>
      <p>DIRECCIÓN OFICIAL: Morelos #703<br>
        COLONIA: Centro<br>
        C.P 68000<br>
       </p>   <hr>
     </div>

<div class="col-lg-6 col-lg-offset-0 col-md-6 col-md-offset-0 direc">
      <p><strong>C.P. CLAUDIA IVETTE DIAZ MEOÑO</strong>
        <em>JEFE DE LA UNIDAD DE TESORERÍA.</em></p>
      <p>TELÉFONO OFICIAL: 5016267 EXT. 122<br>
        CORREO ELECTRÓNICO OFICIAL:  <a href="mailto:utesoreria@montedepiedad.gob.mx">utesoreria@montedepiedad.gob.mx</a></p>
      <p>DIRECCIÓN OFICIAL: Morelos #703<br>
        COLONIA: Centro<br>
        C.P 68000<br>
       </p>   <hr>
     </div>

<div class="col-lg-6 col-lg-offset-0 col-md-6 col-md-offset-0 direc">

      <p><strong>.</strong>
        <em>JEFE DEL DEPARTAMENTO DE CAJA Y BANCOS.</em></p>
      <p>TELÉFONO OFICIAL: 5016267 EXT. 122<br>
       CORREO ELECTRÓNICO OFICIAL:  <a href="mailto:cajaybancos@montedepiedad.gob.mx">cajaybancos@montedepiedad.gob.mx</a></p>
      <p>DIRECCIÓN OFICIAL: Morelos #703<br>
        COLONIA: Centro<br>
        C.P 68000<br>
       </p>   <hr>
     </div>


<div class="col-lg-6 col-lg-offset-0 col-md-6 col-md-offset-0 direc">

      <p><strong>C.P. ESMERALDA ANGÉLICA RAMÍREZ MENDOZA.</strong><em>JEFA DEL DEPARTAMENTO DE CONTABILIDAD.</em></p>
      <p>TELÉFONO OFICIAL: 5016267 EXT. 117<br>
    CORREO ELECTRÓNICO OFICIAL:  <a href="mailto:contabilidad@montedepiedad.gob.mx">contabilidad@montedepiedad.gob.mx</a></p>
      <p>DIRECCIÓN OFICIAL: Morelos #703<br>
        COLONIA: Centro<br>
        C.P 68000<br>
       </p>   <hr>
     </div>

<div class="col-lg-6 col-lg-offset-0 col-md-6 col-md-offset-0 direc">

      <p><strong>M.I. ANA ESTELA VEGA MARTÍNEZ</strong><em>JEFA DEL DEPARTAMENTO DE RECURSOS HUMANOS.</em></p>
      <p>TELÉFONO OFICIAL: 5016267 EXT. 109<br>
        CORREO ELECTRÓNICO OFICIAL:  <a href="mailto:rhumanos@montedepiedad.gob.mx">rhumanos@montedepiedad.gob.mx</a></p>
      <p>DIRECCIÓN OFICIAL: Morelos #703<br>
        COLONIA: Centro<br>
        C.P 68000<br>
       </p>   <hr>
     </div>


<div class="col-lg-6 col-lg-offset-0 col-md-6 col-md-offset-0 direc">

      <p><strong>LI. RUBEN REYES RAMÍREZ.</strong><em>JEFE DEL DEPARTAMENTO DE INFORMÁTICA.</em></p>
        <p>TELÉFONO OFICIAL: 951 5010600,        5016267 EXT. 136<br>

     CORREO ELECTRÓNICO OFICIAL:  <a href="mailto:informatica@montedepiedad.gob.mx">informatica@montedepiedad.gob.mx</a></p>
      <p>DIRECCIÓN OFICIAL: Morelos #703<br>
        COLONIA: Centro<br>
        C.P 68000<br>
       </p>   <hr>
</div>

<div class="col-lg-6 col-lg-offset-0 col-md-6 col-md-offset-0 direc">
       <p><strong> </strong><em>JEFE DEL DEPARTAMENTO DE SUPERVISIÓN. </em></p>
      <p>TELÉFONO OFICIAL: 5016267 EXT. 106<br>
     CORREO ELECTRÓNICO OFICIAL:  <a href="mailto:usucursales@montedepiedad.gob.mx">usucursales@montedepiedad.gob.mx</a></p>
      <p>DIRECCIÓN OFICIAL: Morelos #703<br>
        COLONIA: Centro<br>
        C.P 68000<br>
       </p>   <hr>
 </div>

<div class="col-lg-6 col-lg-offset-0 col-md-6 col-md-offset-0 direc">
   <p><strong>LIC. EDITH DIEGO MARTÍNEZ. </strong><em>JEFA DE LA UNIDAD DE CONTROL INTERNO.</em></p>
        <p>TELÉFONO OFICIAL: 5016267 EXT. 116<br>
        CORREO ELECTRÓNICO OFICIAL:<a href="fiscalizacion@montedepiedad.gob.mx">fiscalizacion@montedepiedad.gob.mx</a></p>
       <p> DIRECCIÓN OFICIAL: Morelos #703<br>
        COLONIA: Centro<br>
        C.P 68000<br>
       </p>   <hr>
</div>

<div class="col-lg-6 col-lg-offset-0 col-md-6 col-md-offset-0 direc">
        <p><strong>LIC. MONICA MEDINA ALCAYDE</strong><em>JEFA DEL DEPARTAMENTO DE ARCHIVO GENERAL</em></p>
      <p>TELÉFONO OFICIAL: 5016267 EXT. 121<br>
        CORREO ELECTRÓNICO OFICIAL: <a href="mailto:archivogeneral@montedepiedad.gob.mx">archivogeneral@montedepiedad.gob.mx</a></p>
      <p>DIRECCIÓN OFICIAL: Morelos #703<br>
        COLONIA: Centro<br>
        C.P 68000<br>
       </p>   <hr>
     </div>

<div class="col-lg-6 col-lg-offset-0 col-md-6 col-md-offset-0 direc">
        <p><strong>C. FLORENCIA GALLARDO MARTÍNEZ</strong><em>GERENTE DE OFICINA MATRIZ.</em></p>
      <p>TELÉFONO OFICIAL: 5016267 EXT. 118<br>
        CORREO ELECTRÓNICO OFICIAL: <a href="mailto:matriz@montedepiedad.gob.mx">matriz@montedepiedad.gob.mx</a></p>
      <p>DIRECCIÓN OFICIAL: Morelos #703<br>
        COLONIA: Centro<br>
        C.P 68000<br>
       </p>   <hr>
     </div>



<div class="col-lg-6 col-lg-offset-0 col-md-6 col-md-offset-0 direc">

      <p><strong>ARQ. IGNACIO GUTIÉRREZ CRUZ.</strong><em>JEFE DEL DEPARTAMENTO DE SERVICIOS GENERALES.</em></p>
      <p>TELÉFONO OFICIAL: 5016267 EXT. 113<br>
        CORREO ELECTRÓNICO OFICIAL:  <a href="mailto:serviciosgenerales@montedepiedad.gob.mx">serviciosgenerales@montedepiedad.gob.mx</a></p>
      <p>DIRECCIÓN OFICIAL: Morelos #703<br>
        COLONIA: Centro<br>
        C.P 68000<br>
       </p>   <hr>
     </div>



<div class="col-lg-6 col-lg-offset-0 col-md-6 col-md-offset-0 direc">

      <p><strong>LIC. MARTHA NEREYDA LÓPEZ VERGARA.</strong><em>JEFE DE LA UNIDAD DE SUCURSALES.</em></p>
      <p>TELÉFONO OFICIAL: 5016267 EXT. 105<br>
    CORREO ELECTRÓNICO OFICIAL:  <a href="mailto:usucursales@montedepiedad.gob.mx">usucursales@montedepiedad.gob.mx</a></p>
      <p>DIRECCIÓN OFICIAL: Morelos #703<br>
        COLONIA: Centro<br>
        C.P 68000<br>
       </p>   <hr>

     </div>

<div class="col-lg-6 col-lg-offset-0 col-md-6 col-md-offset-0 direc">

      <p><strong>LIC. LUIS MIGUEL RAMÍREZ DIAZ</strong><em>JEFE DEL DEPARTAMENTO DE CONTROL Y AUDITORÍA.</em></p>
      <p>TELÉFONO OFICIAL: 5016267 EXT. 116<br>
        CORREO ELECTRÓNICO OFICIAL: <a href="fiscalizacion@montedepiedad.gob.mx">fiscalizacion@montedepiedad.gob.mx</a></p>
      <p>DIRECCIÓN OFICIAL: Morelos #703<br>
        COLONIA: Centro<br>
        C.P 68000<br>
       </p>   <hr>

     </div>





        </div>
        <div class="tab-pane" id="2b">
        <div class=" col-lg-12"  style="height: 100px; background:#24A093; padding-top: 2%; margin-bottom: 30px; text-align: center"><h2>Valles</h2></div>

     <div class="col-lg-8 col-lg-offset-2 col-md-12 direc">

     <p><strong>C.P. ELIZABETH ANTONIA BARROSO LUIS.</strong><em>GERENTE DE LA SUCURSAL 2  CENTRAL DE ABASTOS.</em></p>
      <p>TELÉFONO OFICIAL: 5165071<br>
        CORREO ELECTRÓNICO OFICIAL: <a href="mailto:abastos@montedepiedad.gob.mx">abastos@montedepiedad.gob.mx</a></p>
      <p>DIRECCIÓN OFICIAL: CALLE VALERIO TRUJANO NUM. 801 <br>
        COLONIA: Centro<br>
        C.P 68000<br>
       </p>   <hr>
        <p><strong>MERC. IRIS RUBI  MALDONAD CABALLERO.</strong><em>GERENTE DE LA SUCURSAL NO. 6 XOXOCOTLÁN.</em></p>
      <p>TELÉFONO OFICIAL: 9515172850<br>
       CORREO ELECTRÓNICO OFICIAL: <a href="mailto:xoxocotlan@montedepiedad.gob.mx  ">xoxocotlan@montedepiedad.gob.mx </a></p>
          </p>
      <p>DIRECCIÓN OFICIAL: PRIMERA PRIVADA DE PROGRESO NO. 100 ESQ. BOULEVARD GUADALUPE HINOJOSA DE MURAT, LOCAL 03 COLONIA PALESTINA SANTA CRUZ XOXOCOTLÁN<br>
        C.P 71980<br>
        </p>   <hr>
         <p><strong>C.V. REYNA PACHECO PACHECO.</strong><em>GERENTE DE LA SUCURSAL  NO. 08 20 DE NOVIEMBRE</em></p>
      <p>TELÉFONO OFICIAL ­: 9515144059<br>
        CORREO ELECTRÓNICO OFICIAL: <a href="mailto:20denoviembre@montedepiedad.gob.mx">20denoviembre@montedepiedad.gob.mx</a></p>
      <p>DIRECCIÓN OFICIAL: CALLE 20 DE NOVIEMBRE 707 B ENTRE ZARAGOZA Y ARISTA COL. CENTRO<br>
        FRENTE AL HOTEL POSADA DEL CARMEN Y ELEKTRA.<br>
        C.P, 68000<br>
        </p><hr>
          <p><strong>C. OLLIVER DAVID RAMOS SILVA.</strong><em>GERENTE DE LA SUCURSAL NO. 12 MODULO AZUL.</em></p>
      <p>TELÉFONO OFICIAL 5187204<br>
       CORREO ELECTRÓNICO OFICIAL: <a href="mailto:moduloazul@montedepiedad.gob.mx">moduloazul@montedepiedad.gob.mx</a></p>
      <p>DIRECCIÓN OFICIAL: BLVD MARTIRES DE CHICAGO Y AVE. PROLETARIADO MEXICANO FOVISSSTE<br>
        C.P 68020<br>
        </p>   <hr>

        <p><strong>LIC. JESÚS RAFAEL ESPAÑA GARCÍA.</strong><em>GERENTE DE LA SUCURSAL 16 REFORMA.</em></p>
      <p>TELÉFONO OFICIAL: 5025219<br>
        CORREO ELECTRÓNICO OFICIAL: <a href="mailto:reforma@montedepiedad.gob.mx">reforma@montedepiedad.gob.mx</a></p>
      <p>DIRECCIÓN OFICIAL: HEROICO COLEGIO MILITAR NUM. 525 COLONIA REFORMA ENTRE SABINOS Y PALMERAS.<br>
        POBLACIÓN: OAXACA.<br>
        C.P 68050<br>
        </p>   <hr>

      <p><strong>C.P. CRISTIAN GABRIEL CARRASCO RODRIGUEZ</strong><em>GERENTE DE LA SUCURSAL NO. 18 CIUDAD JUDICIAL.</em></p>
      <p>TELÉFONO OFICIAL: 5015000 EXT. 27024<br>
        CORREO ELECTRÓNICO OFICIAL: <a href="mailto:cdjudicial@montedepiedad.gob.mx">cdjudicial@montedepiedad.gob.mx</a></p>
      <p>DIRECCIÓN OFICIAL: NÚCLEO DE SERVICIO AURORA PÉREZ, AV. GERARDO PANDAL GRAFF NUM. 1, REYES MANTECON, SAN BARTOLO COYOTEPEC, OAXACA. CENTRO ADMINISTRATIVO DEL PODER EJECUTIVO Y JUDICIAL&rdquo;GENERAL PORFIRIO DIAZ, SOLDADO DE LA PATRIA&rdquo; <br>
        C.P. 71257<br>
        </p>   <hr>

          <p><strong>C. CESAR ALAN LÓPEZ LÓPEZ</strong><em>GERENTE DE LA SUC. 21 TLACOLULA.</em></p>
      <p>TELÉFONO OFICIAL: 56-2-03-09</p>
     <p> CORREO ELECTRÓNICO OFICIAL: <a href="mailto:tlacolula@montedepiedad.gob.mx">tlacolula@montedepiedad.gob.mx</a></p>
       <p> DIRECCIÓN OFICIAL: CALLE 2 DE ABRIL NÚMERO 20 ESQUINA CALLE VICENTE GUERRERO,<br>
        TLACOLULA DE MATAMOROS <br>
        C.P. 70900<br>
        </p>   <hr>

      <p><strong>LIC. AMADITA HERRERA RIOS.</strong><em>GERENTE DE LA SUCURSAL 23 MORELOS.</em></p>
      <p>TELÉFONO OFICIAL 951-5142195<br>
        CORREO ELECTRÓNICO OFICIAL: <a href="mailto:ucreditos@montedepiedad.gob.mx">ucreditos@montedepiedad.gob.mx</a></p>
      <p>DIRECCIÓN OFICIAL: Morelos #703 ALTOS<br>
        COLONIA: Centro<br>
        C.P 68000<br>
        </p><hr>
  </div>

        </div>







        <div class="tab-pane" id="3b">

        <div class=" col-lg-12"  style="height: 100px; background:#F5780F; padding-top: 2%; margin-bottom: 30px; text-align: center"><h2>Istmo</h2></div>
     <div class="col-lg-8 col-lg-offset-2 col-md-12 direc">

<p><strong>LIC. ALFREDO MARTINEZ MATEO</strong><em>GERENTE DE LA SUCURSAL NO. 1 TEHUANTEPEC.</em></p>
      <p>TELÉFONO OFICIAL  9717150082<br>
        CORREO ELECTRÓNICO OFICIAL: <a href="mailto:tehuantepec@montedepiedad.gob.mx">tehuantepec@montedepiedad.gob.mx</a></p>
      <p>DIRECCIÓN OFICIAL: AV. JUANA C. ROMERO NO. 18 BARRIO SAN SEBASTIÁN<br>
        COLONIA: Centro<br>
        POBLACION: SANTO DOMINGO TEHUANTEPEC<br>
        C.P 70760<br>
       </p>   <hr>
         <p><strong>C. CARLOS GALLARDO RAMÍREZ.</strong><em>GERENTE DE LA SUCURSAL  NO. 7 SALINA CRUZ.</em></p>
      <p>TELÉFONO OFICIAL ­: 9717203026<br>
        CORREO ELECTRÓNICO OFICIAL: <a href="mailto:salinacruz@montedepiedad.gob.mx">salinacruz@montedepiedad.gob.mx</a></p>
      <p>DIRECCIÓN OFICIAL: AVENIDA MANUEL ÁVILA CAMACHO N0. 415<br>
        COLONIA: CENTRO<br>
        POBLACION: SALINA CRUZ<br>
        C.P 70600<br>
        </p>   <hr>
        <p> <strong>C. MIGUEL HERNÁNDEZ FUENTES. </strong><em>GERENTE DE LA SUCURSAL NO. 11 IXTEPEC.</em></p>
      <p>TELÉFONO OFICIAL: 9717131567<br>
         CORREO ELECTRÓNICO OFICIAL: <a href="mailto:ixtepec@montedepiedad.gob.mx">ixtepec@montedepiedad.gob.mx</a></p>
      <p>DIRECCIÓN OFICIAL: CALLE MORELOS NO. 34<br>
        COLONIA ESTACIÓN <br>
        POBLACIÓN: CIUDAD IXTEPEC<br>
        C.P 70110<br>
        </p>   <hr>
        <p><strong>LIC. EVA ROJAS JIMÉNEZ.</strong><em>GERENTE DE LA SUCURSAL NO. 13 JUCHITÁN.</em></p>
      <p>TELÉFONO OFICIAL: 9717113289<br>
        CORREO ELECTRÓNICO OFICIAL: <a href="mailto:juchitan@montedepiedad.gob.mx">juchitan@montedepiedad.gob.mx</a></p>
      <p>DIRECCIÓN OFICIAL: CALLE EFRAIN R GÓMEZ 15 C<br>
        COLONIA CENTRO<br>
        POBLACIÓN: JUCHITÁN DE ZARAGOZA<br>
        C.P 70000<br>
        </p>   <hr>
      <p><strong>C. REYNA VALDIVIESO JIMÉNEZ. </strong><em>GERENTE DE LA SUCURSAL 14 JUCHITÁN.</em></p>
      <p>TELÉFONO OFICIAL: 9712810862<br>
        CORREO ELECTRÓNICO OFICIAL: <a href="mailto:juchitan14@montedepiedad.gob.mx">juchitan14@montedepiedad.gob.mx</a></p>
      <p>DIRECCIÓN OFICIAL: AVENIDA HIDALGO NO. 64 A <br>
        COLONIA CENTRO<br>
        POBLACIÓN: JUCHITÁN DE ZARAGOZA<br>
        C.P 70000<br>
        </p><hr>
      <p><strong>&nbsp;</strong></p>
      <p><strong>C.P. DAYSI MIJANGOS RÍOS. </strong><em>GERENTE DE LA SUCURSAL 15 MATIAS ROMERO.</em></p>
      <p>TELÉFONO OFICIAL: 9727220972<br>
        CORREO ELECTRÓNICO OFICIAL: <a href="mailto:matiasromero@montedepiedad.gob.mx">matiasromero@montedepiedad.gob.mx</a></p>
      <p>DIRECCIÓN OFICIAL: AYUNTAMIENTO SIN NÚMERO  ENTRE MORELOS Y 5 DE MAYO SUR<br>
        COLONIA CENTRO<br>
        POBLACIÓN: MATIAS ROMERO<br>
        C.P 70300<br>
        </p><hr>
             <p><strong>C. DONAJÍ LIMA CABRERA.</strong>
        <em>GERENTE  DE LA SUCURSAL 17 SAN BLAS.</em></p>
      <p>TELÉFONO OFICIAL: 9717152720<br>
        CORREO ELECTRÓNICO OFICIAL: <a href="mailto:sanblas@montedepiedad.gob.mx">sanblas@montedepiedad.gob.mx</a></p>
      <p>DIRECCIÓN OFICIAL: CALLE FRANCISCO CORTÉZ, COL. CENTRO ENTRE BENITO JUÁREZ Y MATEO JIMÉNEZ.<br>
        POBLACIÓN: SAN BLAS ATEMPA.<br>
        C.P 70786<br>
        </p>   <hr>
                    </div>
                    </div>


          <div class="tab-pane" id="4b">

          <div class=" col-lg-12"  style="height: 100px; background:#4F9815; padding-top: 2%; margin-bottom: 30px; text-align: center"><h2>Costa</h2></div>

     <div class="col-lg-8 col-lg-offset-2 col-md-12 direc">


<p><strong>L.I. JORGE ULISES ROJAS BARETE</strong><em>GERENTE DE LA SUCURSAL NO. 5 PUERTO ESCONDIDO.</em></p>
      <p>TELÉFONO OFICIAL: 9545821711<br>
       CORREO ELECTRÓNICO OFICIAL: <a href="mailto:puertoescondido@montedepiedad.gob.mx">puertoescondido@montedepiedad.gob.mx</a></p>
      <p>DIRECCIÓN OFICIAL: CALLE 5ª NORTE ESQ. 2ª PONIENTE SECTOR JUÁREZ<br>
        POBLACIÓN: PUERTO ESCONDIDO<br>
        C.P 71980<br>
        </p>   <hr>

        <p><strong>L.A. REYNALDO EDGAR FERNÁNDEZ BAÑOS</strong><em>GERENTE DE LA SUCURSAL NO. 10 PINOTEPA NACIONAL.</em></p>
      <p>TELÉFONO OFICIAL: 9545432702<br>
        CORREO ELECTRÓNICO OFICIAL: <a href="mailto:pinotepa@montedepiedad.gob.mx">pinotepa@montedepiedad.gob.mx</a></p>
        <p>DIRECCIÓN OFICIAL: AVENIDA JUÁREZ NO. 217 ESQ. AV. PÉREZ GAZGA<br>
        COLONIA CENTRO<br>
        C.P 71600<br>
        POBLACIÓN: SANTIAGO PINOTEPA NACIONAL<br>
        </p>   <hr>

         <p><strong>C. JUANA CONTRERAS RAMOS.</strong><em>GERENTE DE LA SUCURSAL NO. 19 POCHUTLA.</em></p>
      <p>TELÉFONO OFICIAL: 019585840370</p>
      <p>CORREO ELECTRÓNICO OFICIAL: <a href="mailto:pochutla@montedepiedad.gob.mx">pochutla@montedepiedad.gob.mx</a></p>
      <p>DIRECCIÓN OFICIAL: AV. LÁZARO CARDENAS ESQ. CALLE LAS PALMAS 17, SECCIÓN PRIMERA <br>
      COLONIA: CENTRO<br>
      C.P. 70900<br>
      POBLACIÓN:SAN PEDRO, POCHUTLA<br>
        </p>   <hr>
            </div>
                        </div>


        <div class="tab-pane" id="5b">
        <div class=" col-lg-12"  style="height: 100px; background:#B4106B; padding-top: 2%; margin-bottom: 30px; text-align: center"><h2>Cuenca</h2></div>
     <div class="col-lg-8 col-lg-offset-2 col-md-12 direc">



   <p><strong>LIC. OMAR ALEXANDER ILLESCAS DELGADO.</strong><em>GERENTE DE LA SUCURSAL 4 TUXTEPEC.</em></p>
      <p>TELÉFONO OFICIAL  2878754055<br>
        CORREO ELECTRÓNICO OFICIAL: <a href="mailto:tuxtepec@montedepiedad.gob.mx">tuxtepec@montedepiedad.gob.mx</a></p>
      <p>DIRECCIÓN OFICIAL: AV. 20 DE NOVIEMBRE No. 170 <br>
        COLONIA: CENTRO<br>
        POBLACIÓN: SAN JUAN BAUTISTA TUXTEPEC<br>
        C.P 68300<br>
         <hr>

      <p><strong>IS.C. EVER FRANCISCO SIMON DOMÍNGUEZ.</strong><em>GERENTE DE LA SUCURSAL NO.9 LOMA BONITA.</em></p>
      <p>TELÉFONO OFICIAL: 2818720003<br>
        CORREO ELECTRÓNICO OFICIAL: <a href="mailto:lomabonita@montedepiedad.gob.mx">lomabonita@montedepiedad.gob.mx</a></p>
        <p>DIRECCIÓN OFICIAL: CALLE MICHOACÁN NO 25 LOCAL 3  <br>
        COLONIA: CENTRO<br>
        POBLACIÓN: LOMA BONITA<br>
        C.P 68400<br>
        </p>   <hr>       </div>  </div>




          <div class="tab-pane" id="7b">
          <div class=" col-lg-12"  style="height: 100px; background:#568FEB; padding-top: 2%; margin-bottom: 30px; text-align: center"><h2>Mixteca</h2></div>

     <div class="col-lg-8 col-lg-offset-2 col-md-12 direc">


 <p><strong>L.E. CLAUDIA SELENE AGUIRRE ROBLES.</strong><em>GERENTE DE LA SUCURSAL 3 HUAJUAPAN.</em></p>
      <p>TELÉFONO OFICIAL  9535321624<br>
        CORREO ELECTRÓNICO OFICIAL: <a href="mailto:huajuapan@montedepiedad.gob.mx">huajuapan@montedepiedad.gob.mx</a></p>
      <p>DIRECCIÓN OFICIAL: CALLE CUAUHTEMOC NO. 16 <br>
        COLONIA: CENTRO<br>
        POBLACIÓN: HUAJUÁPAN DE LEÓN<br>
        C.P 69000
<br>
       </p>   <hr>

      <p><strong>C. REYNA LAZO MARTÍNEZ.</strong><em>GERENTE DE LA SUCURSAL 20 TLAXIACO.</em></p>
      <p>TELÉFONO OFICIAL: 01953-55-2-11-79</p>
     <p> CORREO ELECTRÓNICO OFICIAL: <a href="mailto:tlaxiaco@montedepiedad.gob.mx ">tlaxiaco@montedepiedad.gob.mx </a></p>
        <p>DIRECCIÓN OFICIAL: CALLE ALDAMA NUM.6 ENTRE LAS CALLES INDEPENDENCIA E HIDALGO <br>
        COLONIA: CENTRO<br>
        POBLACIÓN: HEROICA CIUDAD DE TLAXIACO <br>
        C.P. 70900<br>
        </p>   <hr>         </div></div>
      </div>
  </div>












  </div>
</div>



  </body>

</html>

@endsection

@include('plantilla.footerPrincipal')
