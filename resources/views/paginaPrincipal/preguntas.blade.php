@extends('main')
@include('plantilla.headerPrincipal')

@section('contenido')

       <div class="container">

       <div class="row">
          <div class="col-lg-12"><img class="img-responsive" src="img/banners/faqs.jpg" /></div>
        </div>
<div class="row">
<div style="margin-top: 30px;" class="col-lg-8 panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h2 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">P= ¿QUÉ NECESITO PARA EMPEÑAR UN AUTOMOVIL O UNA MOTOCICLETA</a> </h2>
    </div>
    <div id="collapse1" class="panel-collapse collapse in">
      <div class="panel-body"><strong>R=</strong>Los autos deben ser no mayores a 8 años de antigüedad; tener la factura original; presentar las tenencias pagadas, principalmente.</div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h2 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
        ¿QUÉ NECESITO PARA EMPEÑAR?</a>
      </h2>
    </div>
    <div id="collapse2" class="panel-collapse collapse">
      <div class="panel-body">Sólo basta que traigas tu credencial para votar con fotografía vigente o cualquier otra identificación oficial vigente</div>
    </div>
  </div>  <div class="panel panel-default">
    <div class="panel-heading">
      <h2 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse8">
        ¿CUÁNTO ME PRESTAN POR UN EMPEÑO?</a>
      </h2>
    </div>
    <div id="collapse8" class="panel-collapse collapse">
      <div class="panel-body">En alhajas, te prestamos hasta un 85% del valor de tu prenda. En artículos varios, hasta un 65% del valor de tu prenda y, en autos, hasta un 50% del valor del vehículo.</div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h2 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
        ¿QUÉ PLAZO MANEJAN EN EL EMPEÑO?</a>
      </h2>
    </div>
    <div id="collapse3" class="panel-collapse collapse">
      <div class="panel-body">  En alhajas tenemos 2 plazos:
12 semanas y 4 semanas.<br>
En artículos varios,
desde cuatro hasta 12 semanas.<br>
En autos, manejos un plazo de 4 semanas.</div>
    </div>
  </div>
  
  <div class="panel panel-default">
    <div class="panel-heading">
      <h2 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
        ¿QUÉ TASA DE INTERES ME OTORGAN?</a>
      </h2>
    </div>
    <div id="collapse4" class="panel-collapse collapse">
      <div class="panel-body">  Te ofrecemos una tasa de interés<strong> desde el 1%</strong></div>
    </div>
  </div>
  
  <div class="panel panel-default">
    <div class="panel-heading">
      <h2 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">
        LLEGUÉ AL FINAL DEL PLAZO MARCADO EN LA BOLETA, ¿QUÉ PUEDO HACER?</a>
      </h2>
    </div>
    <div id="collapse5" class="panel-collapse collapse">
      <div class="panel-body">  Te ofrecemos refrendos ilimitados en alhajas o abonos a capital para que no pierdas tu prenda.</div>
    </div>
  </div>
  
  <div class="panel panel-default">
    <div class="panel-heading">
      <h2 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">
        NO PUDE REFRENDAR NI HACERLE ALGUN ABONO ABONO A MI BOLETA, ¿PIERDO MI PRENDA?</a>
      </h2>
    </div>
    <div id="collapse6" class="panel-collapse collapse">
      <div class="panel-body">  No. Si no pudiste realizar ningún movimiento, te otorgamos hasta 15 días de gracia, después de tu fecha de vencimiento del plazo para que puedas refrendar o abonar tu boleta.</div>
    </div>
  </div>
  
  <div class="panel panel-default">
    <div class="panel-heading">
      <h2 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse7">
        TUVE UN CONTRATIEMPO Y NO REALICÉ NINGÚN MOVIMIENTO A MI PRENDA DENTRO DEL PERIODO DE GRACIA, ¿PIERDO MI PRENDA?</a>
      </h2>
    </div>
    <div id="collapse7" class="panel-collapse collapse">
      <div class="panel-body">  No. Puedes acudir, antes de que termine el periodo de gracia, a la sucursal donde realizaste el empeño y solicitar un resguardo por 7 días a tu prenda</div>
    </div>
  </div>
</div>
<div class="col-lg-4">
  <div class="col-lg-10 col-lg-offset-2 articulos-empe"><h2>¿Sabes qué artículos puedes empeñar?</h2>
  <a class="articulos-boton"href="{{url('/articulos')}}">conÓcelos</a>
  </div>
 <div class="col-lg-10 col-lg-offset-2 articulos-empe"><h2>¿Conoces las ventajas de empeñar con nosotros?</h2>
  <a class="articulos-boton"href="{{url('/ventajas')}}">conÓcelos</a>
  </div>
</div>
</div>
   
    </div>
    
   @endsection

@include('plantilla.footerPrincipal')

   	
   		</body>

</html>
