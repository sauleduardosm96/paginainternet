@extends('main')
@include('plantilla.headerPrincipal')

@section('contenido')
<div class="container">


  <div class="row">
   <div class=" col-lg-12"  style="height: 100px; background:#f5780f; padding-top: 2%; margin-bottom: 30px; text-align: center">
     <h2>BILLETE CONMEMORATIVO.</h2></div>
  </div>





  <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
    <li data-target="#carousel-example-generic" data-slide-to="3"></li>
    <li data-target="#carousel-example-generic" data-slide-to="4"></li>
    <li data-target="#carousel-example-generic" data-slide-to="5"></li>
    <li data-target="#carousel-example-generic" data-slide-to="6"></li>
    <li data-target="#carousel-example-generic" data-slide-to="7"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">

<div class="col-lg-8 col-lg-offset-2 historia" style="text-align: justify;"  >
  <img class="img-responsive img-full" src="img/boleto.jpg" alt="">
  <br>
  Billete conmemorativo del "86 aniversario del monte de piedad del estado de Oaxaca". Sorteo mayor a celebrarse el martes 20 de agosto de 2019 a las 20 horas.
    Un premio en 3 series de 6,000,000.00 cada una. Adquiére tu billete en el punto de venta de tu preferencia o en las sucursales de esta Institución.


<!--
<h5>GRAN SORTEO DEL MONTE DE PIEDAD DEL ESTADO DE OAXACA </h5>


<p>VIGENCIA DEL PERMISO: Del 18 de febrero del año 2019 al 10 de mayo del año 2019.</p>

<p>VIGENCIA DE LA PROMOCIÓN: Del 18 de febrero del año 2019 al 05 de mayo del año 2019.</p>

<p>El sorteo se realizara únicamente en el área geográfica correspondiente al Estado de Oaxaca.  </p>

<p>Se emitirán un total de ciento veinticinco mil (125,000) boletos, la emisión de los folios será consecutivo, del folio número 000001 al 125000.  </p>

 <p>Los premios consisten en: un primer premio por un vehículo marca Volkswagen modelo Gol HB TRENDLINE STD 2019 con un valor de $198,090.00 (Ciento Noventa y Ocho Mil Noventa Pesos 00/100 M.N.) y un segundo premio consistente en una motocicleta marca Italika modelo DS125 con un valor de $21,110.00 (Veintiún Mil Ciento Diez Pesos 00/100 M.N.). </p>

<p>Las bases y condiciones del presente sorteo se publicaran en el portal institucional del Monte de Piedad del Estado de Oaxaca: http://montedepiedad.gob.mx, en la oficina matriz y y/o en cualquiera de las 22 Sucursales del Monte de Piedad del Estado de Oaxaca.</p>

<h5>CONDICIONES DE PARTICIPACIÓN: </h5>

<p>Únicamente participaran las personas que realicen empeños y/o refrendos de alhajas y unidades de motor (automóviles y motocicletas) en la Oficina Matriz y/o en cualquiera de las 22 Sucursales del Monte de Piedad del Estado de Oaxaca; el cliente podrá obtener un boleto para participar en el GRAN SORTEO DEL MONTE DE PIEDAD DEL ESTADO DE OAXACA. </p>

<p>Participan la Oficina Matriz y las siguientes sucursales del Monte de Piedad del Estado de Oaxaca: número 1 “Tehuantepec; número 2 “Central de Abastos”; número 3 “Huajuapan”; número 4 “Tuxtepec”; número 5 “Puerto Escondido”; número 6 “Xoxocotlán”; número 7 “Salina Cruz”; número 8 “20 de Noviembre”; número 9 “Loma Bonita”; número 10 “Pinotepa Nacional”; número 11 “Ixtepec”; número 12 “Modulo Azul”; número 13 “Juchitán”; número 14 “Juchitán”; número 15 “Matías Romero”; número 16 “Reforma”; número 17 “San Blas”; número 18 “Ciudad Judicial”; número 19 “Pochutla”; número 20 “Tlaxiaco”; número 21 “Tlacolula” y; número 22 “Miahuatlán”.</p>

<p>Se otorgará un boleto por pignorante que empeñe desde $1,000.00 (MIL PESOS 00/100 M.N.) hasta $ 24,999.99 (VEINTICUATRO MIL NOVECIENTOS NOVENTA Y NUEVE PESOS 99/100 M.N.) por operación al día.</p>

<p>Se otorgarán tres (3) boletos por pignorante que realice un empeño igual o superior a los $ 25,000.00 (VEINTICINCO MIL PESOS 00/100 M.N.) por operación al día.</p>

<p>Se otorgará un (1) boleto por pignorante que su importe de refrendo sea de $200.00 (DOSCIENTOS PESOS 00/100 M.N) hasta 4,999.99 (CUATRO MIL NOVECIENTOS NOVENTA Y NUEVA 99/100 M.N.) por operación al día.</p>

<p>Se otorgarán dos (2) boletos por pignorante que su importe de refrendo sea igual o superior a $5,000.000 (CINCO MIL PESOS 00/100 M.N.), por operación al día.
</p>




<h5>ESTRUCTURA DE LOS PREMIOS: </h5>

<p>El valor total en premios para este sorteo es de $219,200.00 (Doscientos Diecinueve Mil Doscientos Pesos 00/100 M.N.) dicho monto incluye el Impuesto al Valor Agregado (IVA)</p>







	<p>ORDEN DE PREMIACIÓN: 1 </p>
	<p>CANTIDAD:  1</p>
	<p>DESCRIPCIÓN DEL PREMIO: Un automóvil marca Volkswagen modelo Gol HB TRENDLINE STD 2019.  </p>
	<p>VALOR UNITARIO: $198,090.00</p>
	<p>ORDEN DE PREMIACIÓN: 2 </p>
	<p>CANTIDAD:  1</p>
	<p>DESCRIPCIÓN DEL PREMIO: Una motocicleta marca Italika modelo DS125.  </p>
	<p>VALOR UNITARIO: $21,110.00</p>
	<p>VALOR TOTAL  $219,200.00     </p>







<h5>MECÁNICA DEL CONCURSO: </h5>

<p>Las fechas para el depósito de los boletos en las tómbolas serán del 18 de febrero del año 2019 al 05 de mayo del 2019. Será responsabilidad de los clientes verificar que sus boletos cumplan con los requisitos de participación y registro para ser acreedores al premio de conformidad con lo que se establece en las presentes bases; en caso de duda, el cliente deberá acudir a la Oficina Matriz o Sucursal más cercana del Monte de Piedad del Estado de Oaxaca.</p>

<p>El participante deberá acudir a la Oficina Matriz y/o sucursal más cercana del Monte de Piedad del Estado de Oaxaca a realizar su empeño o para que se le proporcione el boleto respectivo. </p>

<p>Es indispensable que el cliente registre en el boleto los siguientes datos: </p>
	<p>•	Nombre completo.</p>
	<p>•	Dirección.</p>
	<p>•	Población.</p>
	<p>•	Teléfono </p>
	<p>•	Email. </p>
	<p>•	Número de Boleta.</p>
	<p>•	Número de Sucursal.</p>



<p>La falta de cualquiera de los datos antes señalados, impedirá ser acreedores de los premios establecidos.</p>

<p>Entre más boletos deposite, más oportunidades tendrá de ganar.</p>

<p>Los boletos deberán ser depositados en las urnas que para tal efecto se colocaran en la Oficina Matriz y en cada una de las 22 Sucursales con las que cuenta el Monte de Piedad del Estado de Oaxaca.</p>

<h5>SELECCIÓN DEL GANADOR: </h5>

<p>Los boletos participarán en el GRAN SORTEO DEL MONTE DE PIEDAD DEL ESTADO DE OAXACA a celebrarse a las 12 horas del día 10  de mayo del 2019 en la Oficina Matriz del Monte de Piedad del Estado de Oaxaca ubicada en  Avenida Morelos número setecientos tres, esquina con Calle Macedonio Alcalá, Colonia Centro, Oaxaca de Juárez, Oaxaca, C.P. 68000. La entrada al sorteo el libre y gratuita.
El mismo día del sorteo, es decir, a las 11:30 horas del día 10 de mayo del 2019, se concentraran en una sola tómbola los boletos que contengan las 23 urnas, mismas que fueron distribuidas en la Oficina Matriz y en cada una de las 22 Sucursales con las que cuenta el Monte de Piedad del Estado de Oaxaca. </p>

<p>Para seleccionar a los ganadores se hará girar la tómbola y se detendrá cuando lo indique el Inspector de la Secretaria de Gobernación. De la tómbola se extraerán 2 boletos los cuales corresponderán a los ganadores del sorteo, la información del boleto será cotejada con los talonarios de los folios generados. Una vez que se tengan los números de folios ganadores se procederá a exhibirlos al público como los números ganadores. </p>

<p>El primer boleto que se obtenga de los concentrados de la urna, será el ganador un vehículo marca Volkswagen modelo Gol HB TRENDLINE STD 2019 con un valor de $198,090.00 (Ciento Noventa y Ocho Mil Noventa Pesos 00/100 M.N.), dicho monto incluye el Impuesto al Valor Agregado (IVA). (Uno por ganador). </p>

<p>El segundo boleto que se obtenga de los concentrados de la urna, será el ganador una motocicleta marca Italika modelo DS125 con un valor de $21,110.00 (Veintiún Mil Ciento Diez Pesos 00/100 M.N.), dicho monto incluye el Impuesto al Valor Agregado (IVA). (Uno por ganador). </p>

<p>El mismo día del sorteo se contactara a los ganadores directamente al teléfono que hayan registrado en el boleto y/o se les enviará un correo electrónico. </p>

<p>Los ganadores contaran con 20 días hábiles contados a partir de la fecha del sorteo para comunicarse con el Monte de Piedad del Estado de Oaxaca, de lo contrario perderán su derecho para reclamarlo.
</p>
<p>Los ganadores podrán solicitar la entrega de sus premios en un horario de 9:00 a las 16:00 horas en el domicilio ubicado en Avenida Morelos número setecientos tres, esquina con Calle Macedonio Alcalá, Colonia Centro, Oaxaca de Juárez, Oaxaca, C.P. 68000.</p>

<p>La entrega del premio será en la fecha que establezca el “Monte de Piedad del Estado de Oaxaca”, que no puede ser mayor a 20 días hábiles de la fecha en la cual se realizó el sorteo. </p>


<h5>PUBLICACIÓN DEL GANADOR: </h5>

<p>Los ganadores serán publicados el día 11 de mayo del año 2019, en las cartulinas que se instalaran en la Oficina Matriz y en cada una de las 22 Sucursales con las que cuenta el Monte de Piedad del Estado de Oaxaca y/o en el portal de internet: http://montedepiedad.gob.mx; así como el día 12 de mayo del año 2019 en el Periódico el “Imparcial de Oaxaca” y en el Periódico “Noticias Voz e Imagen de Oaxaca”. </p>

<h5>RESTRICCIONES:</h5>

	<p>•	El ganador deberá conservar su boleto porque sin él no podrá reclamar su premio; el mismo caduca a los 20 días hábiles a partir de la fecha del sorteo. </p>

	<p>•	No se podrá sobrescribir, tachar y/o corregir en el cupón, ni escribir con dos tintas distintas, ya que en caso de que el participante lo deposite en la urna con alguna de estas características será automáticamente descalificado.</p>

	<p>•	En caso de que no se llenen todos los campos que contiene el boleto, de acuerdo a lo estipulado en las bases, será motivo de descalificación.</p>

	<p>•	El premio otorgado no podrá ser devuelto en ninguna situación.</p>

	<p>•	Únicamente participan las boletas de empeños o refrendos expedidas del día 18 de febrero del año 2019 al 05 de mayo del 2019 realizados en la Oficina Matriz y/o 22 Sucursales del Monte de Piedad del Estado de Oaxaca.</p>

	<p>•	No participan las operaciones de abono y/o desempeño.</p>

	<p>•	Al momento de realizarse el sorteo, la(s) boleta(s) no deberán(n) estar vencida(s).</p>

	<p>•	Las boletas de empeños y/o refrendos solo serán acumulables por operación y por día.  </p>

	<p>•	El premio no podrá ser rembolsable por dinero en efectivo, ni sustituido por bien o servicio diferente al ofrecido.</p>

	<p>•	No podrán participar empleados del Monte de Piedad del Estado de Oaxaca, así como aquellas personas que directa o indirectamente hayan participado en este sorteo.</p>

	<p>•	Los participantes deberán proporcionar sus datos personales completos y de forma verdadera al momento de llenar su boleto, ya que en caso de resultar ganadores, el premio les será entregado únicamente si los documentos con los que se identifican, corresponden a los datos proporcionados.</p>

<h5>LEGALES:</h5>

<p>El responsable del presente sorteo es el Monte de Piedad del Estado de Oaxaca.</p>

<p>En caso de robo del boleto, se deberá presentar la denuncia correspondiente ante el Ministerio Público, en caso de extravío se deberá dar aviso por escrito a la autoridad correspondiente.</p>

<p>Los premios serán pagados de conformidad con lo establecido en las bases del sorteo.</p>

<p>Para cualquier aclaración o información referente a este sorteo o resultados del mismo comunicarse a los teléfonos (951) 501 62 67 extensión 124 o acudir al siguiente domicilio Avenida Morelos número setecientos tres, esquina con Calle Macedonio Alcalá, Colonia Centro, Oaxaca de Juárez, Oaxaca, C.P. 68000.</p>

<p>En caso de queja, acudir a la Dirección General de Juegos y Sorteos de la Secretaria de Gobernación, ubicada en Versalles número 49, piso 2, Colonia Juárez, Alcaldía Cuauhtémoc, C.P. 06600, Ciudad de México o bien comunicarse al teléfono  52098800.</p>

<p>Conserve su comprobante no debe presentar enmendaduras, raspaduras o alteración alguna, en caso contrario el poseedor no tendrá derecho a reclamar el premio. </p>

<p>El Monte de Piedad del Estado de Oaxaca se reserva el derecho de rechazar o excluir a cualquier participante durante el sorteo por no reunir con los requisitos descritos en las presentes bases y términos de participación, o contravenga las normas o finalidad del concurso.</p>

<p>El Monte de Piedad del Estado de Oaxaca no se responsabiliza de las posibles pérdidas de datos por problemas de correo electrónico y/o por el mal funcionamiento del internet.</p>

<p>Todos los participantes autorizan el uso de los datos proporcionados, así como las imágenes, videos y fotos que se tomen en la entrega del premio y durante el desarrollo del sorteo para ser utilizadas para contenido en redes sociales, televisión, radio y prensa por parte del Monte de Piedad del Estado de Oaxaca.</p>

<p>Los participantes podrán consultar el Aviso de Privacidad en http://montedepiedad.gob.mx/terminos</p>
-->
</div>

    </div>




  </div>

  <!-- Controls -->

</div>









</div>
    @endsection

@include('plantilla.footerPrincipal')

	</body>

</html>
