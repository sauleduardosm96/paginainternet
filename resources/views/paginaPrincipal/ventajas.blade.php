@extends ('main')
@include('plantilla.headerPrincipal')

@section('contenido')


       
       <div class="container">

       <div class="row">
          <div class="col-lg-12"><img class="img-responsive" src="img/banners/ventajas.jpg" /></div>
        </div>
       <div class="row">
  <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12" style="height: 100px;background:#24a093;padding-top: 20px; ">
    <h2 class="text-center">Empeñar con nosotros te ofrece muchas ventajas</h2>
    
  </div>
       </div>
  <div class="row">
    <div class="col-lg-10 col-lg-offset-1 arti">
      <div class="row" style="margin-top: 20px">
              <div class="col-lg-2"><img class="img-responsive quempenar" src="img/iconos/iconos-ventajas_0001_004-business.png"/></div>
        <div class="col-lg-4">
          <h3>Facilidades de pago</h3>
                    
                <p>Puedes refrendar o abonar tu boleta en cualquier momento mientras la prenda se encuentre en depositaría. <br><br>
Una vez hecho el movimiento (refrendo o abono) se te entrega nueva boleta con el mismo plazo que la boleta original.</p>
                <p>Los abonos o refrendos son ilimitados.</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
              </div>
              <div class="col-lg-2"><img class="img-responsive quempenar" src="img/iconos/iconos-ventajas_0003_002-business-1.png"/></div>
<div class="col-lg-4">
          <h3>Seguridad</h3>
                <p>La seguridad de que tu prenda siempre estará bien resguardada. </p>
                <div>
                  <p><br>
                    Tienes hasta 15 días de gracia después del vencimiento de tu boleta y si  necesitas <strong>más</strong> tiempo te ofrecemos hasta 7 días <strong>más </strong>de resguardo.</p>
                </div>
                <p>&nbsp;</p>
              </div>
      </div><div class="row" style="margin-top: 20px">
              <div class="col-lg-2"><img class="img-responsive quempenar" src="img/iconos/iconos-ventajas_0000_005-package.png"/></div>
        <div class="col-lg-4">
          <h3>Queremos que tu prenda vuelva a ti</h3>
                    
                <p>
Tienes la oportunidad de refrendar en Almoneda. </p>
                <p>
                  <br>
En caso de que desempeñes, te entregamos tu prenda de inmediato.<br>
                </p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
        </div>
              <div class="col-lg-2"><img class="img-responsive quempenar" src="img/iconos/iconos-ventajas_0004_001-check.png"/></div>
<div class="col-lg-4">
          <h3>Préstamos justos</h3>
                <p>En el servicio de empeño en alhajas puedes decidir el préstamo máximo desde un 75 hasta un 85% de préstamo sobre el valor de tu prenda a un plazo a elegir de 4 a hasta 12 semanas.<br>
                  <br>
No te subimos a buró de crédito. </p>
        </div>
      </div>
          <div></div>
                	
          </div>
  </div>

		   
     
         
@endsection

@include('plantilla.footerPrincipal')
    		</body>

</html>		