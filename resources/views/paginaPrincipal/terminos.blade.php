@extends('main')
@include('plantilla.headerPrincipal')

@section('contenido')
<div class="container">

 
  <div class="row">
   <div class=" col-lg-12"  style="height: 100px; background:#f5780f; padding-top: 2%; margin-bottom: 30px; text-align: center">
     <h2>AVISO DE PRIVACIDAD</h2></div>
  </div>
  
  
  
  
  
  <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
    <li data-target="#carousel-example-generic" data-slide-to="3"></li>
    <li data-target="#carousel-example-generic" data-slide-to="4"></li>
    <li data-target="#carousel-example-generic" data-slide-to="5"></li>
    <li data-target="#carousel-example-generic" data-slide-to="6"></li>
    <li data-target="#carousel-example-generic" data-slide-to="7"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">

<div class="col-lg-8 col-lg-offset-2 historia" style="text-align: justify;"  >
      
    
<h5>I. DOMICILIO DEL RESPONSABLE.</h5>
<p >El Monte de Piedad del Estado de Oaxaca, con domicilio en Avenida Morelos número 703, colonia Centro, Oaxaca de Juárez, Centro, Oaxaca, C.P. 68000, es responsable de la protección y tratamiento de los datos personales que recabe, en términos de los artículos 6 y 16 segundo párrafo de la Constitución Política de los Estados Unidos Mexicanos, 3° de la Constitución Política del Estado Libre y Soberano de Oaxaca, la  Ley General de Transparencia y Acceso a la Información Pública, Ley de Transparencia y Acceso a la Información Pública para el Estado de Oaxaca y la Ley General de Datos Personales en Posesión de Sujetos Obligados (LGDPPSO).</p>
<h5>II. LOS DATOS PERSONALES QUE SERÁN SOMETIDOS A TRATAMIENTO.</h5> 
<p>Para llevar a cabo las finalidades descritas en el presente aviso de privacidad podremos utilizar datos personales de identificación, contacto, referencias laborales y personales, lugar o medio para practicar alguna notificación, datos adicionales de contacto, patrimoniales y financieros, condición particular o requerimientos de accesibilidad del titular (algunos de éstos, en ciertos contextos podrían ser considerados sensibles),  así como imágenes y sonidos para la seguridad de nuestras instalaciones.</p>
<h5>III. FUNDAMENTO LEGAL PARA LLEVAR A CABO EL TRATAMIENTO.</h5> 
<p>Lo anterior, tiene su  fundamento en los artículos 23, 24, 25, 45, 60, 68  y 121 de la Ley General de Transparencia y Acceso a la Información Pública; 1, 4, 6, 7, 17, 18, 19, 20, 21, 22, 23, 25, 26 y 70 de la Ley General de Protección de Datos Personales en Posesión de Sujetos Obligados.</p>     
<h5>IV. FINALIDADES DEL TRATAMIENTO.</h5>
<p>Los datos personales que recabamos y los que son proporcionados por sus titulares se utilizaran para efecto de dar trámite al servicio o solicitud que corresponda; así mismo, podrán ser utilizados para generar estadísticas e informes, actividades con fines mercadológicos, de prospección comercial de nuevos productos y la seguridad de nuestras instalaciones. </p> 
<p>Sus datos personales podrán ser transferidos para cumplir con lo dispuesto en la ley; así también, se podrán transferir a organismos públicos con el objetivo de cumplir con las finalidades por las cuales ha proporcionado sus datos personales; a las aseguradoras contratadas por esta Institución para el seguimiento a pólizas de seguro y; para atender requerimientos de autoridades competentes.</p> 
<p>De manera particular, le informamos que los datos personales recibidos con motivo de las solicitudes de acceso a la información o para el ejercicio de los derechos de acceso, rectificación, cancelación y oposición al tratamiento de datos personales (ARCO) son utilizados para dar atención a la solicitud correspondiente.</p> 
<h5>V. MEDIOS Y PROCEDIMIENTOS PARA EL EJERCICIO DE LOS DERECHOS ARCO.</h5>
<p>La Ley General de Protección de Datos Personales en Posesión de Sujetos Obligados (LGPDPPSO) prevé a favor de los titulares de datos personales los derechos de acceso, rectificación, cancelación y oposición al tratamiento de estos (derechos ARCO). Excepto en aquellos casos en que se actualice algunos de los supuestos previstos en el artículo 55 de dicho ordenamiento.</p>
<p>Para el ejercicio de cualquiera de los derechos ARCO, se deberá acreditar la identidad del titular y en su caso la personalidad de quien actúe como representante de este. La solicitud respectiva podrá ser presentada ante la Unidad de Transparencia, por escrito o a través del portal de la Plataforma Nacional de Transparencia. El procedimiento y los requisitos para el ejercicio de los derechos ARCO, se encuentran previstos en el Título Tercero de la Ley General de Protección de Datos Personales en Posesión de Sujetos Obligados. Sin perjuicio de ello el interesado podrá ponerse en contacto con el personal de la Unidad de Transparencia, que lo atenderá y orientara al respecto.</p>   
<h5>VI. DOMICILIO DE LA UNIDAD DE TRANSPARENCIA.</h5>
<p>Para todos los efectos relativos al presente aviso de privacidad y las demás obligaciones previstas en la Ley General de Protección de Datos Personales en Posesión de los Sujetos Obligados, le informamos que el domicilio de la Unidad de Transparencia del Monte de Piedad del Estado de Oaxaca es el ubicado en Avenida Morelos, número 703, colonia centro, Oaxaca de Juárez, Centro, Oaxaca, C.P. 68000, teléfono 5016267 extensión 124 y la dirección de correo electrónico de la misma es u.transparencia.montedepiedad@gmail.com.</p>
<h5>VII. MEDIOS A TRAVÉS DE LOS CUALES SE COMUNICARÁN LOS CAMBIOS EN EL AVISO DE PRIVACIDAD.</h5>
<p>El presente aviso de privacidad puede sufrir modificaciones, cambios o actualizaciones, derivadas de nuevos requerimientos, de nuestras propias atribuciones, de nuestras prácticas de privacidad o por otras causas. El Monte de Piedad del Estado de Oaxaca informará sobre los cambios que tenga este aviso, a través de nuestro sitio web <a href="http://montedepiedad.gob.mx">http://montedepiedad.gob.mx</a></p>
    </div>
      
    </div>
   
    
     
    
  </div>

  <!-- Controls -->
 
</div>
  
  
  
  
  
  
  
  

</div>
    @endsection

@include('plantilla.footerPrincipal')
   
	</body>

</html>



   
